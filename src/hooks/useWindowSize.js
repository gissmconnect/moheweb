import React, { useState, useEffect, useLayoutEffect } from 'react'

export default function useWindowSize() {
  const [size, setSize] = useState(0)
  const useEnhancedEffect =
    typeof window !== 'undefined' ? useLayoutEffect : useEffect
  useEnhancedEffect(() => {
    function updateSize() {
      setSize(window.innerWidth)
    }
    window.addEventListener('resize', updateSize)
    updateSize()
    return () => window.removeEventListener('resize', updateSize)
  }, [])
  console.log('isMobileisMobileisMobile')
  return size
}
