export const getJid = (jid='') => {
    return jid.split('/')[0]
}
  
export const getUsername = (jid='') => {
    return jid.split('@')[0]
}
  