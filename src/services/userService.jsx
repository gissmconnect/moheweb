import { SERVER_URL } from "../config";
import { handleResponse } from "./fack.backend";

export const UserLogin = async (email, password) => {
    const data = {
        email, password
    }
    try {
        await fetch(`${SERVER_URL}/agent/login`, {
            method: "POST",
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(handleResponse)
            .then(user => {
                localStorage.setItem('token', user.token);
                localStorage.setItem('role', user.data.role);
                localStorage.setItem('user_id', user.data._id);
                localStorage.setItem('email',user.data.email);
                window.location.href = `${process.env.PUBLIC_URL}/dashboard/`
                return user;
            })
    } catch (error) {
        console.log(error)
    }
}