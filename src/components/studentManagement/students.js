import React, { Fragment,useEffect,useState } from 'react';
import Breadcrumb from '../../layout/breadcrumb'
import { Container, Row, Col, Card, CardHeader, CardBody, CardFooter, Media, Form, FormGroup, Label, Input, Button } from 'reactstrap'
import axios from 'axios'
import {  StudentsTableHeader,Edit,Update,Delete} from '../../constant'

const StudentView = (props) => {

  const data = [
    {
      no: "1",
      name: "ahmedabri1",
      email: "ahmedabri@gmail.com",
      contact:"056423447",
      college:"Sultan Qaboos University (SQU)",
      course: "Law"
    },
    {
      no: "2",
      name: "ahmed2",
      email: "ahmed2@gmail.com",
      contact:"056423447",
      college:"Ibri College of Applied Sciences",
      course: "B.Sc"
    },
    {
      no: "3",
      name: "ahme4",
      email: "ahme4@gmail.com",
      contact:"056423447",
      college:"Rustaq College of Education",
      course: "BBA"
    },
    {
      no: "4",
      name: "ahmedab4",
      email: "ahmedab4@gmail.com",
      contact:"056423447",
      college:"Ibra Technical College",
      course: "MCA"
    },
    {
      no: "5",
      name: "ahmedabri1",
      email: "ahmedabri@gmail.com",
      contact:"056423447",
      college:"College of Shariaa Sciences",
      course: "Arts & Social Sciences"
    },
  ]

  return (
    <Fragment>
      <Breadcrumb parent="Apps" title="Student Management" />
      <Container fluid={true}>
        <div className="edit-profile">
          <Row>
            <Col md="12">
              <Card>
                {/* <CardHeader>
                  <h4 className="card-title mb-0">User Management</h4>
                 
                </CardHeader> */}
                <div className="table-responsive">
                  <table className="table card-table table-vcenter text-nowrap">
                    <thead>
                      <tr>
                        {StudentsTableHeader.map((items,i) => 
                          <th key={i}>{items}</th>
                        )}
                      </tr>
                    </thead>
                    <tbody>

                      {data.map((items,i) => 
                        <tr key={i}>
                          <td><a className="text-inherit">{items.no} </a></td>
                          <td>{items.name}</td>
                          <td>{items.email}</td>
                          <td>{items.contact}</td>
                          <td>{items.college}</td>
                          <td>{items.course}</td>
                          {/* <td className="text-right">
                            <Button color="primary" size="sm"><i className="fa fa-pencil"></i> {Edit}</Button>
                            <Button color="transparent" size="sm"><i className="fa fa-link"></i> {Update}</Button>
                            <Button color="danger" size="sm"><i className="fa fa-trash"></i> {Delete}</Button>
                          </td> */}
                        </tr>
                      )}
                      
                    </tbody>
                  </table>
                </div>
              </Card>
            </Col>
          </Row>
        </div>
      </Container>
    </Fragment>
  );
}

export default StudentView;