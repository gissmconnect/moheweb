import React, { Fragment,useEffect,useState,Component, forwardRef } from 'react';
import Breadcrumb from '../../layout/breadcrumb'
import { Container, Row, Col, Card, CardHeader, CardBody, CardFooter, Media, Form, FormGroup, Label, Input, Button } from 'reactstrap'
import { AddBox, ArrowDownward } from "@material-ui/icons";
import MaterialTable, { MTableEditField } from "material-table";
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import CircularProgress from "@material-ui/core/CircularProgress";
import {getStudents} from "services/studentService"

const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
  };

const StudentView = (props) => {
const [studentsList, setStudentsList] = useState([])
useEffect(() => {
  getStudentsList();
}, []);
 const getStudentsList = async () => {
    try {
      let res = await getStudents();
      if (res.length > 0) {
        setStudentsList(res)
      }
    } catch (error) {
      console.log(error)
    }
  };

  

  return (
    <Fragment>
      <Breadcrumb parent="Apps" title="Student Management" />
      <Container fluid={true}>
        <div className="edit-profile">
        <Row>
            <Col md="12">
            <div className="table-responsive">
        <MaterialTable
              icons={tableIcons}
              columns={[
                { title: "Roll No", field: "roll_no",
                
                headerStyle: {
                  zIndex:0
                } },
                {title: 'Name', field: 'name', headerStyle: {
                  zIndex:0
                }},
                {title: 'ENroll.No', field: 'enroll_no', headerStyle: {
                  zIndex:0
                }},
                {title: 'Course', field: 'course', headerStyle: {
                  zIndex:0
                }}
              ]}
              data={studentsList.map((item) => {
                return {
                  name: item.name,
                  enroll_no:item.enroll_no,
                  roll_no:item.roll_no,
                  course:item.course
                }
              })}
              title= ""
              options={{
                actionsColumnIndex: -1,
                addRowPosition: 'first'
              }}
              // editable={{
              //   onRowAdd: newData =>
              //     new Promise((resolve, reject) => {
              //       setTimeout(() => {
              //         setData([...data, newData]);
                      
              //         resolve();
              //       }, 1000)
              //     }),
              //   onRowUpdate: (newData, oldData) =>
              //     new Promise((resolve, reject) => {
              //       setTimeout(() => {
              //         const dataUpdate = [...data];
              //         const index = oldData.tableData.id;
              //         dataUpdate[index] = newData;
              //         setData([...dataUpdate]);
        
              //         resolve();
              //       }, 1000)
              //     }),
              //   onRowDelete: oldData =>
              //     new Promise((resolve, reject) => {
              //       setTimeout(() => {
              //         const dataDelete = [...data];
              //         const index = oldData.tableData.id;
              //         dataDelete.splice(index, 1);
              //         setData([...dataDelete]);
        
              //         resolve();
              //       }, 1000)
              //     }),
              // }}
              
            />
            </div>
            </Col>
            </Row>
        </div>
      </Container>
    </Fragment>
  );
}

export default StudentView;