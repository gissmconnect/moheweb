import React, { Fragment, useEffect, useState } from "react";
import Breadcrumb from "../../layout/breadcrumb";
import Ckeditor from "react-ckeditor-component";
import { Typeahead } from "react-bootstrap-typeahead";
import Dropzone from "react-dropzone-uploader";
import Parse, { convertNodeToElement } from "react-html-parser";
import { toast } from "react-toastify";
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
  Dropdown,
  DropdownMenu,
  DropdownToggle,
  Toast,
  Badge,
} from "reactstrap";
import {
  PostEdit,
  Title,
  Type,
  Category,
  Content,
  Post,
  Discard,
  Text,
  Audio,
  Video,
  Image,
  SelectCard,
} from "../../constant";
import { X } from "react-feather";

import { postKBData, updateData } from "../../services/kbService";

const AddQuestion = (props) => {
  const { categoryType, getKbData, setAdd, setEdit, item } = props;
  const [postTitle, setPostTitle] = useState(item ? item.kb_title : "");
  const [content, setContent] = useState(item ? item.kb_body : "");
  const [selectedCategory, setSelectedCategory] = useState(
    item ? item.category : ""
  );
  const [keywords, setKeywords] = useState(item && item.kb_keywords ? item.kb_keywords.split(",") : []);
  const [keywordValue, setKeywordValue] = useState("");
  const onChange = (e) => {
    setContent(e.editor.getData());
  };

  const onTitleChange = (e) => {
    setPostTitle(e.target.value);
  };

  const onArrayChange = (e) => {
    setKeywords(e.target.value);
  };

  const handleDataUpdate = async () => {
    try {
      let keyword = "";
      if (keywords.length > 0) {
        keyword = keywords.join();
      }
      let res = await updateData(
        item._id,
        selectedCategory,
        postTitle,
        content,
        keyword,
        true
      );
      // console.log(res, "res--------------");
      if (res.success === true) {
        getKbData();
        setEdit(false);
        setPostTitle("");
        setSelectedCategory("");
        setContent("");
        setKeywords([]);
        setKeywordValue("");
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleDataAdd = async () => {
    let errorList = [];
    if (!postTitle) {
      toast.error("Title field could not be empty");
      errorList.push("Please enter Title");
    } else if (!selectedCategory) {
      toast.error("category Type field could not be empty");
      errorList.push("Please enter category Type");
    } else if (!content) {
      toast.error("content could not be empty");
      errorList.push("Please enter content");
    }

    if (errorList.length === 0) {
      try {
        let keyword = "";
        if (keywords.length > 0) {
          keyword = keywords.join();
        }
        let res = await postKBData(
          selectedCategory,
          postTitle,
          content,
          keyword,
          true
        );
        if (res) {
          setPostTitle("");
          setSelectedCategory("");
          setContent("");
          setKeywords([]);
          setKeywordValue("");
          getKbData();
          setAdd(false);
          setEdit(false);
        }
      } catch (error) {
        console.log(error);
      }
    }
  };

  const handleRemoveKeyword = (index) => {
    const keyword = [...keywords];
    keyword.splice(index, 1);
    setKeywords(keyword);
  };

  return (
    <Fragment>
      <Container fluid={true}>
        <Row>
          <Col sm="12">
            <Card>
              <CardHeader>
                <h5>{item ? "Edit Question" : "Add Question"} </h5>
              </CardHeader>
              <CardBody className="add-post">
                <Form className="row needs-validation">
                  <Col sm="12">
                    <FormGroup>
                      <Label for="validationCustom01">{Title}:</Label>
                      <Input
                        className="form-control"
                        id="validationCustom01"
                        type="text"
                        value={postTitle}
                        onChange={onTitleChange}
                        placeholder="Post Title"
                        required=""
                      />
                      <div className="valid-feedback">{"Looks good!"}</div>
                    </FormGroup>
                    <FormGroup>
                      <Label for="validationCustom01">{Category}:</Label>

                      <Input
                        type="select"
                        name="select"
                        id="exampleSelect"
                        value={selectedCategory}
                        onChange={(e) => setSelectedCategory(e.target.value)}
                      >
                        <option value="">---Please Select Category---</option>
                        {categoryType.map((item, index) => {
                          return (
                            <option key={index} value={item._id}>
                              {item.type}
                            </option>
                          );
                        })}
                      </Input>
                    </FormGroup>
                    <FormGroup>
                      <Label for="validationCustom01">keywords:</Label>
                      <Input
                        className="form-control"
                        id="validationCustom01"
                        type="text"
                        value={keywordValue}
                        onKeyPress={(e) => {
                          if (e.charCode === 13) {
                            let keyArray = [...keywords];
                            keyArray.push(keywordValue);
                            setKeywords(keyArray);
                            setKeywordValue("");
                          }
                        }}
                        onChange={(e) => setKeywordValue(e.target.value)}
                        placeholder="Please enter keywords"
                      />
                      <div className="valid-feedback">{"Looks good!"}</div>
                    </FormGroup>
                    <FormGroup>
                      <div style={{ display: "flex", flexWrap: "wrap" }}>
                        {keywords.map((item, index) => {
                          return (
                            <Badge
                              key={index}
                              color="primary"
                              style={{
                                fontSize: 16,
                                display: "flex",
                                alignItems: "center",
                                marginBottom: 6,
                              }}
                            >
                              {item}
                              <X
                                size={"30px"}
                                style={{ width: "20px", height: "20px", marginRight:0, position: "static" }}
                                onClick={() => handleRemoveKeyword(index)}
                              />
                            </Badge>
                          );
                        })}
                      </div>
                    </FormGroup>
                    {/* <FormGroup>
                      <div className="col-form-Label">
                        {Category}:
                        <Typeahead
                          id="multiple-typeahead"
                          className="mt-2"
                          clearButton
                          labelKey="name"
                          options={categoryList}
                          placeholder="Select Question Category...."
                        />
                      </div>
                    </FormGroup> */}
                    <div className="email-wrapper">
                      <div className="theme-form">
                        <FormGroup>
                          <Label>{Content}:</Label>
                          <Ckeditor
                            activeclassName="p10"
                            content={content}
                            events={{
                              change: onChange,
                            }}
                          />
                        </FormGroup>
                      </div>
                    </div>
                  </Col>
                </Form>
                <div className="btn-showcase">
                  {item ? (
                    <>
                      <Button
                        color="primary"
                        type="button"
                        onClick={handleDataUpdate}
                      >
                        Update
                      </Button>
                      <Button
                        color="light"
                        type="reset"
                        onClick={() => {
                          setEdit(false);
                        }}
                      >
                        {Discard}
                      </Button>
                    </>
                  ) : (
                    <>
                      <Button
                        color="primary"
                        type="submit"
                        onClick={handleDataAdd}
                      >
                        {/* onSubmit={handleDataAdd} */}
                        Add
                      </Button>
                      <Button
                        color="light"
                        type="reset"
                        onClick={() => {
                          setPostTitle("");
                          setSelectedCategory("");
                          setContent("");
                          setKeywords([]);
                          setKeywordValue("");
                          setAdd(false);
                          setEdit(false);
                        }}
                      >
                        {Discard}
                      </Button>
                    </>
                  )}
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </Fragment>
  );
};

export default AddQuestion;
