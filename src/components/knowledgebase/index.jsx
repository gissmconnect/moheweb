import React, { Fragment, useState, useEffect } from "react";
import Breadcrumb from "../../layout/breadcrumb";
import {
  Codepen,
  FileText,
  Youtube,
  BookOpen,
  Aperture,
  Archive,
  ArrowRight,
  Search,
  Edit,
} from "react-feather";
import two from "../../assets/images/faq/2.jpg";
import one from "../../assets/images/faq/1.jpg";
import three from "../../assets/images/faq/3.jpg";
import four from "../../assets/images/faq/4.jpg";
import Parse, { convertNodeToElement } from "react-html-parser";
import errorImg from "../../assets/images/search-not-found.png";
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Media,
  Form,
  FormGroup,
  Input,
  Button,
  Label
} from "reactstrap";
import axios from "axios";
import {
  Articles,
  Knowledgebase,
  Support,
  BrowseArticles,
  FeaturedTutorials,
  WebDesign,
  WebDevelopment,
  UIDesign,
  UXDesign,
} from "../../constant";
import AddPost from "./addQuestion";
import { getKB, getKbCategory, search } from "../../services/kbService";
import { Accordion } from "react-bootstrap";

const KnowledgebaseComponent = () => {
  const role = localStorage.getItem('role')
  const [searchTerm, setSearchTerm] = useState("");
  // const [search, setsearch] = useState([]);
  const [Data, setData] = useState([]);
  const [add, setAdd] = useState(false);
  const [edit, setEdit] = useState(false);
  const [activeEditId, setActiveEditId] = useState("");
  const [kbCategory, setKbCategory] = useState([]);
  const [kbData, setKbData] = useState([]);
  const [Knowledgebase, setKnowledgeBase] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState("");
  const [sorting, setSorting] = useState("")
  useEffect(() => {
    // axios
    //   .get(`${process.env.PUBLIC_URL}/api/knowledgebaseDB.json`)
    //   .then((res) => setsearch(res.data));
    kbCategoryList();
  }, []);
  const getKbData = async () => {
    try {
      let result = await getKB();
      setKbData(result);
    } catch (error) {
      console.log(error);
    }
  };
  const kbCategoryList = async () => {
    try {
      let result = await getKbCategory();
      if (result.length > 0) {
        setKbCategory(result);
        getKbData();
      }
    } catch (error) {
      console.log(error);
    }
  };
  const handleSearch = async () => {
    try {
      let result = await search(searchTerm, sorting, selectedCategory);
      setKbData(result);
    } catch (error) {
      console.log(error)
    }
  }
  useEffect(() => {
    var data = [];
    for (let i = 0; i < kbCategory.length; i++) {
      var obj = {
        category: kbCategory[i].type,
        kb_data: []
      }
      for (let j = 0; j < kbData.length; j++) {
        if (kbCategory[i]._id === kbData[j].category) {
          obj["category"] = kbCategory[i].type
          obj.kb_data.push(kbData[j])
        }
      }
      data.push(obj)
      setKnowledgeBase(data)
    }
  }, [kbData])


  const handleChange = event => {
    // const searchByTitle = [];
    setSearchTerm(event.target.value);
    // axios.get(`${process.env.PUBLIC_URL}/api/knowledgebaseDB.json`).then(res => setData(res.data))
    // Data.filter(data => {
    //     if (data.title.toLowerCase().indexOf((event.target.value).toLowerCase()) > -1) {
    //         searchByTitle.push(data);
    //     }
    //     return data
    // })
    // setsearch(searchByTitle)

  };
  const handleReset = async => {
    setSorting({})
    setSelectedCategory("")
    setSearchTerm("")
    getKbData()

  }
  return (
    <Fragment>
      <Breadcrumb parent="Apps" title="Knowledgebase" />
      <Container fluid={true}>
        <Row>
          <Col xs="12">
            <div className="knowledgebase-bg" style={{ backgroundImage: `url(${require("../../assets/images/knowledgebase/bg_1.jpg")})`, backgroundSize: "cover", backgroundPosition: "center", display: "block" }}>
              <img className="bg-img-cover bg-center" src={require("../../assets/images/knowledgebase/bg_1.jpg")} alt="looginpage" style={{ display: "none" }} />
            </div>
            <div className="knowledgebase-search">
              <div>
                <h3>{"How Can I help you?"}</h3>
                <Form >
                  <FormGroup >
                    <Input  type="text" onChange={handleChange} value={searchTerm} placeholder="Type question here" />
                  </FormGroup>
                </Form>
                <Input type="select"
                      name="select"
                      className="form-control digits"
                      value={sorting}
                      onChange={(e) => {
                        setSorting(e.target.value)
                        console.log(JSON.parse(e.target.value), "e.target")
                      }}
                    >
                      <option value={'{}'}>Sort by</option>
                      <option value={'{"createdAt":-1}'}>Newest First</option>
                      <option value={'{"createdAt":1}'}>Newest last</option>

                    </Input>
                  {/* </FormGroup>
                </Form>

                <Form className="form-inline" action="#" method="get">
                  <FormGroup className="w-100"> */}
                    {/* <Label htmlFor="exampleFormControlSelect9">Category </Label> */}
                    <Input type="select"
                      name="select"
                      style={{ marginTop:10 }}
                      className="form-control digits"
                      value={selectedCategory}
                      onChange={(e) => setSelectedCategory(e.target.value)}
                    >
                      <option value="">Please Select Category </option>
                      {kbCategory.map((item, index) => {
                        return (
                          <option key={index} value={item._id}>
                            {item.type}
                          </option>
                        );
                      })}
                    </Input>
                <Button color="primary" style={{ marginRight: 10, marginTop:10 }} onClick={() => handleSearch()} >Search</Button>
                <Button color="secondary" style={{ marginTop:10 }} onClick={() => handleReset()}>Reset</Button>
              </div>
            </div>
          </Col>
          <Col sm="12">
            <Row>

              <Col sm="12">
                <Card>
                  <CardHeader style={{ justifyContent: 'space-between', display: "flex" }} >
                    <h5>{BrowseArticles}</h5>
                    {role === "Admin" && <Button color="primary" size="sm" onClick={() => setAdd(!add)}>Add</Button>}
                  </CardHeader>
                  {add && <AddPost setAdd={setAdd} add={add} categoryType={kbCategory} getKbData={getKbData} />}
                  <CardBody>
                    <Row className="browse">
                      {Knowledgebase.length > 0 ? Knowledgebase.map((data, i) => {
                        if (data.kb_data.length > 0) {
                          return (
                            <Col xl="4 xl-50" md="6" key={i}>
                              <div className="browse-articles browse-bottom">
                                <h6>
                                  <span><Archive /></span>{data.category}

                                </h6>
                                <ul>
                                  {data.kb_data.length > 0 && data.kb_data.map((item, index) => {
                                    return (
                                      <Accordion key={index} >
                                        <Accordion.Toggle as={Row} eventKey={String(index)}>
                                          <li><a href='#javascript'><span><FileText /></span><span>{item.kb_title}</span></a>
                                            {role === "Admin" && <Edit
                                              style={{
                                                width: 15,
                                                marginLeft: 30,
                                                marginBottom: -5,
                                                zIndex: 111,
                                              }}
                                              onClick={() => {
                                                setEdit(!edit);
                                                setActiveEditId(item._id);
                                              }}
                                            />}
                                            {edit &&
                                              activeEditId === item._id && (
                                                <AddPost
                                                  setEdit={setEdit}
                                                  edit={edit}
                                                  categoryType={kbCategory}
                                                  getKbData={getKbData}
                                                  item={item}
                                                />
                                              )}
                                          </li>
                                        </Accordion.Toggle>
                                        <Accordion.Collapse eventKey={String(index)}>
                                          <div className="htmlparserContent">
                                            {Parse(item.kb_body)}
                                          </div>
                                        </Accordion.Collapse>
                                      </Accordion>
                                    )


                                  })}
                                  {/* <li><a href='#javascript'><span><FileText /></span><span>{data.text1}</span></a></li>
                                                                    <li><a href='#javascript'><span><FileText /></span><span>{data.text2}</span></a></li>
                                                                    <li><a href='#javascript'><span><FileText /></span><span>{data.text3}</span></a></li>
                                                                    <li><a href='#javascript'><span><FileText /></span><span>{data.text4}</span></a></li>
                                                                    <li><a href='#javascript'><span><ArrowRight /></span><span>{data.text5}</span></a></li> */}
                                </ul>
                              </div>
                            </Col>

                          )
                        }

                      }) :
                        <Media className="img-fluid m-auto" src={errorImg} alt="" />
                      }
                    </Row>
                  </CardBody>
                </Card>
              </Col >
            </Row >
          </Col >
        </Row >
      </Container >
    </Fragment >
  );
};

export default KnowledgebaseComponent;
