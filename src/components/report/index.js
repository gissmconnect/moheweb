import React, { Fragment, useEffect, useState, Component, forwardRef } from 'react';
import Breadcrumb from '../../layout/breadcrumb'
import { Container, Row, Col,Modal, ModalHeader, ModalBody, Input,Button } from 'reactstrap'
import { AddBox, ArrowDownward } from "@material-ui/icons";
import MaterialTable, { MTableEditField } from "material-table";
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import { getReports, getPdf, getExcel } from "services/reportService";
import jsPDF from 'jspdf'
import html2canvas from 'html2canvas';
import 'jspdf-autotable'
import { Font } from '../../assets/fonts/Amiri-Regular-normal';
import { toast } from 'react-toastify';

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

const Report = (props) => {
  const [reportsList, setReportsList] = useState([])
  const [dataLength, setDatalength]= useState(0)
  const [Pdfmodal, setPdfmodal] = useState(false);
  const [Excelmodal, setExcelmodal] = useState(false);
  const [pdflimit, setPdflimit] = useState('')
  const [excellimit, setExcellimit] = useState('')
  const PdfModaltoggle = () => setPdfmodal(!Pdfmodal);
  const ExcelModaltoggle = () => setExcelmodal(!Excelmodal);
  useEffect(() => {
    getReportsList();
  }, []);
  const getReportsList = async () => {
    try {
      let res = await getReports();
      if (res.length > 0) {
        setDatalength(res.length)
        setReportsList(res)
      }
    } catch (error) {
      console.log(error)
    }
  };
  const getPdfPrint = async () => {
    setPdfmodal(false);
    try {
      let res = await getPdf(pdflimit);
      window.open(res, "_blank")
      setPdflimit('')
    } catch (error) {
      console.log(error)
    }
  };
  const getExcelPrint = async () => {
    setExcelmodal(false);
    try {
      let res = await getExcel(excellimit);
      window.open(res, "_blank")
      setExcellimit('')
    } catch (error) {
      console.log(error)
    }
  };
 
  return (
    <>
    <Fragment>
      <Breadcrumb parent="Apps" title="Reports" />
      <Container fluid={true}>
        <div className="edit-profile" >
          <Row>
            <Col md="12" id="reports_table">
              <MaterialTable

                icons={tableIcons}
                columns={[
                  {
                    title: "Mobile", field: "from", headerStyle: {
                      zIndex: 0
                    },
                    

                  },
                  {
                    title: 'Query', field: 'text', filtering: false, headerStyle: {
                      zIndex: 0,
                    }
                  },
                  {
                    title: 'Reply', field: 'reply', filtering: false, headerStyle: {
                      zIndex: 0,
                    }
                  },
                  {
                    title: 'Date', field: 'createdAt', headerStyle: {
                      zIndex: 0,
                    }
                  },
                  {
                    title: "ID",
                    field: "_id",
                    hidden: true
                  },
                ]}
                data={reportsList.map((item) => {
                  return {
                    from: item.from,
                    text: item.text,
                    reply: item.reply,
                    createdAt: `${new Date(item.createdAt).toLocaleDateString('en-IN', { year: 'numeric', month: 'numeric', day: 'numeric' })} ${new Date(item.createdAt).toLocaleTimeString('en-IN', { hour: '2-digit', minute: '2-digit' })}`,
                    _id: item._id
                  }
                })}
                title="Reports"
                localization={{
                  toolbar: {
                    exportCSVName: "Export as Excel",
                    exportPDFName: "Export as Pdf"
                  }
                }}
                options={{
                  exportButton: true,
                  exportAllData: true,
                  exportFileName: "reports_data",
                  exportPdf:()=>PdfModaltoggle() ,
                  exportCsv:()=>ExcelModaltoggle(),
                  filtering: true,
                  actionsColumnIndex: -1,
                  addRowPosition: 'first'
                }}

              />
            </Col>
          </Row>
        </div>
      </Container>
    </Fragment>

    <Modal isOpen={Pdfmodal} toggle={PdfModaltoggle} size="sm">
                      <ModalHeader toggle={PdfModaltoggle}>
                        Select no of data to print
                      </ModalHeader>
                      <ModalBody>
                      <Input type="select"
                      name="select"
                      className="form-control digits"
                      value={pdflimit}
                      onChange={(e) => {
                        setPdflimit(e.target.value)
                      }}
                    >
                      <option value={''}>Select no of rows</option>
                      {dataLength > 3 && <option value={Math.ceil(dataLength/4)}>{Math.ceil(dataLength/4)}</option>}
                      {dataLength > 2 && <option value={Math.ceil(dataLength/3)}>{Math.ceil(dataLength/3)}</option>}
                      {dataLength > 1 && <option value={Math.ceil(dataLength/2)}>{Math.ceil(dataLength/2)}</option>}
                      <option value={dataLength}>All rows</option>
                    </Input>
                    <Button color="primary" disabled={pdflimit ? false : true} style={{ marginTop:10 }} onClick={() => getPdfPrint()}>Print Pdf</Button>
                     </ModalBody>
                    </Modal>
                    <Modal isOpen={Excelmodal} toggle={ExcelModaltoggle} size="sm">
                      <ModalHeader toggle={ExcelModaltoggle}>
                        Select no of data to print
                      </ModalHeader>
                      <ModalBody>
                      <Input type="select"
                      name="select"
                      className="form-control digits"
                      value={excellimit}
                      onChange={(e) => {
                        setExcellimit(e.target.value)
                        console.log(e.target.value, "limitttt")
                      }}
                    >
                      <option value={''}>Select no of rows</option>
                      <option value={Math.ceil(dataLength/4)}>{Math.ceil(dataLength/4)}</option>
                      <option value={Math.ceil(dataLength/3)}>{Math.ceil(dataLength/3)}</option>
                      <option value={Math.ceil(dataLength/2)}>{Math.ceil(dataLength/2)}</option>
                      <option value={dataLength}>All rows</option>
                    </Input>
                    <Button color="primary" disabled={excellimit ? false : true} style={{ marginTop:10 }} onClick={() => getExcelPrint()}>Print Excel</Button>
                     </ModalBody>
                    </Modal>
  </>
  );
}

export default Report;