import React, { Fragment, useEffect, useState, Component, forwardRef } from 'react';
import Breadcrumb from '../../layout/breadcrumb'
import { Container, Row, Col, Card, CardHeader, CardBody, CardFooter, Media, Form, FormGroup, Label, Input, Button } from 'reactstrap'
import { AddBox, ArrowDownward } from "@material-ui/icons";
import MaterialTable, { MTableEditField } from "material-table";
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import CircularProgress from "@material-ui/core/CircularProgress";
import { getUsers, AddUser, updateUser, deleteUser } from "services/userManagementService";
import { toast } from 'react-toastify';

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

const UserEdit = (props) => {
  const [usersList, setUsersList] = useState([])
  useEffect(() => {
    getUsersList();
  }, []);
  const getUsersList = async () => {
    try {
      let res = await getUsers();
      console.log(res)
      if (res.length > 0) {
        setUsersList(res)
      }
    } catch (error) {
      console.log(error)
    }
  };
  const handleRowUpdate = async (newData, oldData, resolve, reject) => {
    let errorList = [];
    if (!newData.nickname) {
      errorList.push('name is required')
      toast.error('Name is required')
      reject();
    } else if (!newData.username) {
      errorList.push('name is required')
      toast.error('Username is required')
      reject();
    } else if (!newData.email) {
      errorList.push('email is required')
      toast.error('Email is required')
      reject();
    }
    else if (!newData.role) {
      errorList.push('role is required')
      toast.error('User Role is required')
      reject();
    }
    if (errorList.length === 0) {
      try {
        await updateUser(
          newData.nickname,
          newData.username,
          newData.email,
          newData.role,
          newData._id
        )
        getUsersList();
        resolve()
      } catch (error) {
        console.log(error)
        resolve()
      }
    }
  }
  const handleRowAdd = async (newData, resolve, reject) => {
    console.log("rowadddd")
    let errorList = [];
    if (!newData.nickname) {
      errorList.push('name is required')
      toast.error('Name is required')
      reject();
    } else if (!newData.username) {
      errorList.push('name is required')
      toast.error('Username is required')
      reject();
    } else if (!newData.email) {
      errorList.push('email is required')
      toast.error('Email is required')
      reject();
    }
    else if (!newData.role) {
      errorList.push('role is required')
      toast.error('User Role is required')
      reject();
    }
    if (errorList.length === 0) {
      const nickname = newData.nickname;
      const username= newData.username
      const email = newData.email;
      const password = newData.password;
      const role = newData.role;
      console.log(nickname,username,email,password,role)
      try {
        let res = await AddUser(nickname,username, email, password, role)
        getUsersList();
        resolve()
      }
      catch (error) {
        console.log(error)
        resolve()
      }
    }
  }
  const handleRowDelete = async (oldData, resolve) => {
    try {
      await deleteUser(oldData._id);
      getUsersList();
      resolve();
    } catch (error) {
      console.log(error)
      resolve();
    }
  }
  return (
    <Fragment>
      <Breadcrumb parent="Apps" title="User Management" />
      <Container fluid={true}>
        <div className="edit-profile">
          <Row>
            <Col md="12">
              <MaterialTable
                icons={tableIcons}
                columns={[
                  {
                    title: "Name", field: "nickname", headerStyle: {
                      zIndex: 0
                    }
                  },
                  {
                    title: "Username", field: "username", headerStyle: {
                      zIndex: 0
                    }
                  },
                  {
                    title: 'Email', field: 'email', headerStyle: {
                      zIndex: 0
                    }
                  },
                  {
                    title: 'Role', field: 'role',
                    lookup: { 'Admin': "Admin", 'Agent': "Agent" },
                    headerStyle: {
                      zIndex: 0
                    }
                  },
                  {
                    title: 'Password', field: 'password', headerStyle: {
                      zIndex: 0
                    }
                  },
                  {
                    title: 'Created on', field: 'createdAt', editable: "never", headerStyle: {
                      zIndex: 0
                    }
                  },
                  {
                    title: 'Last Updated', field: 'updatedAt', editable: "never", headerStyle: {
                      zIndex: 0
                    }
                  },


                  {
                    title: "ID",
                    field: "_id",
                    hidden: true
                  },
                ]}
                data={usersList.map((item) => {
                  return {
                    nickname: item.nickname,
                    username:item.username,
                    email: item.email,
                    role: item.role,
                    password:'******',
                    createdAt: `${new Date(item.createdAt).toLocaleDateString('en-IN', { year: 'numeric', month: 'numeric', day: 'numeric' })} ${new Date(item.createdAt).toLocaleTimeString('en-IN', { hour: '2-digit', minute: '2-digit' })}`,
                    updatedAt: `${new Date(item.updatedAt).toLocaleDateString('en-IN', { year: 'numeric', month: 'numeric', day: 'numeric' })} ${new Date(item.updatedAt).toLocaleTimeString('en-IN', { hour: '2-digit', minute: '2-digit' })}`,
                    _id: item._id
                  }
                })}
                title=""
                options={{
                  actionsColumnIndex: -1,
                  addRowPosition: 'first',
                  pageSize:10,
                  pageSizeOptions:[10,50,100,200]
                }}
                editable={{
                  onRowUpdate: (newData, oldData) =>
                    new Promise((resolve, reject) => {
                      handleRowUpdate(newData, oldData, resolve, reject);
                    }),
                  onRowAdd: (newData) =>
                    new Promise((resolve, reject) => {
                      handleRowAdd(newData, resolve, reject)
                    }),
                  onRowDelete: (oldData) =>
                    new Promise((resolve) => {
                      handleRowDelete(oldData, resolve)
                    }),
                }}
              />
            </Col>
          </Row>
        </div>
      </Container>
    </Fragment>
  );
}

export default UserEdit;