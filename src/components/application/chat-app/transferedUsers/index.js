import React, { useEffect, useState } from 'react';
import { TabPane, Spinner } from 'reactstrap';
import { useRef } from 'react';
import { getTransferedQueue, userMapping, getTransferredChatLogs } from 'services/chatService';
import ChatUserAvatar from '../userAvatar';
import { useChatData } from '../chatDataContext';
import { dataMessageBodyHint } from '../../../CustomerChatBoard';

const TransferedUsers = (props) => {
  const { client, currentUser, setFetchingChatLogs, setUpdateChatLogs, onAddActiveUser } = useChatData();
  const [update, setUpdate] = useState(false);
  const [transferedLoading, setTransferedLoading] = useState(false);
  const [intervalApiCall, setIntervalApiCall] = useState(false);
  const [mapping, setMapping] = useState(false);

  const transferedData = useRef([]);
  const transferedUsers = transferedData.current;

  useEffect(() => {
    const interval = setInterval(() => setIntervalApiCall(Math.random), 30000);
    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    if (intervalApiCall) {
      getTransfered();
    } else {
      getTransfered(true);
    }
  }, [intervalApiCall]);

  const fetchChatLogs = (jid) => {
    setFetchingChatLogs(jid);
    console.log('ChatDataProvider fetchChatLogs called', {jid});
    getTransferredChatLogs(jid).then(res => {
      console.log('ChatDataProvider fetchChatLogs res', {res});
      const {data={}} = res;
      const {chatLogs=[]} = data;
      setUpdateChatLogs({messages: chatLogs, jid});
      setFetchingChatLogs(false);
    }).catch(error => {
      setFetchingChatLogs(false);
      console.log('ChatDataProvider fetchChatLogs error', {error})
    })
  }

  const getTransfered = (loading) => {
    setTransferedLoading(loading);
    getTransferedQueue(currentUser.jid)
      .then((res) => {
        const { data = {} } = res;
        const { response = [] } = data;
        transferedData.current = response;
        console.log('ChatDataProvider TransferedUsers getTransfered res', res);
        setTransferedLoading(false);
        setUpdate(Math.random());
      })
      .catch((error) => {
        console.log('ChatDataProvider PendingUsers getTransfered error', error);
        setTransferedLoading(false);
      });
  };

  const sendDataMessage = (to) => { // Change agent on user side
    const data = {
      from: currentUser.jid,
      type: 'transfer'
    }
    const msgData = {
      to,
      body: `${dataMessageBodyHint}${JSON.stringify(data)}`
    }
    client.sendMessage(msgData);
  }

  const startChat = (to) => {
    setMapping(to);
    userMapping(to, currentUser.jid)
      .then((res) => {
        acceptSubscription(to);
        client.subscribe(to);
        console.log('ChatDataProvider startChat res', res);
        setMapping(false);
        removePendingUser(to);
        sendDataMessage(to);
        fetchChatLogs(to);
        onAddActiveUser(to);
      })
      .catch((error) => {
        console.log('ChatDataProvider startChat error', error);
        setMapping(false);
      });
  };

  const removePendingUser = (jid) => {
    const data = transferedData.current;
    const index = data.indexOf(jid);
    data.splice(index, 1);
    setUpdate(Math.random());
  };

  const acceptSubscription = (jid) => {
    client.acceptSubscription(jid);
  };

  return (
    <TabPane tabId="1">
      <div className="people-list">
        <ul className="list digits custom-scrollbar">
          {transferedLoading && !transferedUsers.length ? (
            <div className="d-flex justify-content-center mt-5 pt-5">
              <Spinner type="grow" color="primary" />{' '}
            </div>
          ) : null}
          {transferedUsers.map((user, index) => {
            return (
              <li
                key={index}
                className="clearfix"
                onClick={() => startChat(user.transferuser)}
                style={{ cursor: 'pointer' }}>
                <ChatUserAvatar />
                <div className="about">
                  <div className="name" style={{ lineHeight: '44px' }}>
                    <div>
                      <div>{user.transferuser}</div>
                      <div className="transferred-user-from mr-2">
                        from: {user.previousagent}
                        {mapping === user.transferuser ? <Spinner className="ml-1" size="sm" color="primary" /> : null}
                      </div>
                    </div>
                  </div>
                </div>
              </li>
            );
          })}
          {!transferedUsers.length && !transferedLoading ? (
            <div className="text-center mt-5">No have any transfered user in queue.</div>
          ) : null}
        </ul>
      </div>
    </TabPane>
  );
};

export default TransferedUsers;
