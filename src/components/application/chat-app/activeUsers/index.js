import React, { useEffect, useState } from "react";
import {
  Col,
  Card,
  CardBody,
  Media,
  Form,
  FormGroup,
  Input,
} from "reactstrap";
import errorImg from "assets/images/search-not-found.png";
import { useRef } from "react";
import { CHATSTATES } from "../../../CustomerChatBoard";
import { getJid, getUsername } from "utils/filters";
import UserItem from "./userItem";
import ChatAgentAvatar from "../agentAvatar";
import { useChatData } from '../chatDataContext';

const ActiveUsers = (props) => {
    const {client, activeUsers, connected, selectedUser={}, unreadMessages, setSelectedUser, currentUser} = useChatData();

  const [searchKeyword, setSearchKeyword] = useState("");
  const [update, setUpdate] = useState(false);

  const typingUsersRef = useRef({});
  const typingUsers = typingUsersRef.current;

  console.log('ChatUser ActiveUsers', {unreadMessages})

  useEffect(() => {
    if(connected && client){
      client.on('chat:state', (d) => {
        console.log('ChatUser chat:state', d);
        const jid = getJid(d.from);
        if(d.chatState === CHATSTATES.COMPOSING && jid !== currentUser.jid){
          typingUsersRef.current[jid] = true;
        } else {
          typingUsersRef.current[jid] = false;
        }
        setUpdate(Math.random())
      });
    }
  }, [connected])
  

  useEffect(() => {
    if(!selectedUser || !update) return;
    setSelectedUser({...selectedUser, typing: typingUsers[selectedUser.jid]});
  }, [update])
  
  const handleSearchKeyword = (keyword) => {
    const searchData = activeUsers.filter((member) => {
      return member.username.includes(keyword)
    })
    return searchData;
  };

  const onSelectUser = (user) => {
    setSelectedUser(user);
  }

  const data = searchKeyword ? handleSearchKeyword(searchKeyword) : activeUsers;
  const reverseData = data.reverse();

  return <Col sm="12" className="call-chat-sidebar">
          <Card>
            <CardBody className="chat-body">
              <div className="chat-box">
                <div className="chat-left-aside">
                  <div className="media">
                    <ChatAgentAvatar/>
                    <div className="about">
                      <div className="name f-w-600">{getUsername(currentUser.jid)}</div>
                    </div>
                  </div>
                  <div className="people-list">
                    <div className="search">
                      <Form className="theme-form">
                        <FormGroup className="form-group">
                          <Input
                            className="form-control"
                            type="text"
                            placeholder="search"
                            defaultValue={searchKeyword}
                            onChange={(e) =>
                              setSearchKeyword(e.target.value)
                            }
                          />
                          <i className="fa fa-search"></i>
                        </FormGroup>
                      </Form>
                    </div>
                    {reverseData.length > 0 ? (
                      <ul className="list">
                        {reverseData
                          .map((item, i) => {
                            return (
                              <UserItem onClick={() => onSelectUser(item)} selected={selectedUser.jid === item.jid} key={i} item={item} typing={typingUsers[item.jid]} unreadMessages={unreadMessages[item.jid]}/>
                            );
                          })}
                      </ul>
                    ) : (
                      <Media
                        className="img-fluid m-auto"
                        src={errorImg}
                        alt=""
                      />
                    )}
                  </div>
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
};

export default ActiveUsers;
