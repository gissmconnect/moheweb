import React, { useEffect, useState } from 'react';
import { FETCH_LAST_ACTIVITY_TIMEOUT } from '../../../CustomerChatBoard';
import ChatUserAvatar from '../userAvatar';
import { useChatData } from '../chatDataContext';

const UserItem = (props) => {
  const {client, connected, addActiveUser} = useChatData();
  const { item, selected, unreadMessages } = props;
  const [getUserStatus, setGetUserStatus] = useState(false);

  let interval = null;

  useEffect(() => {
    interval = setInterval(() => setGetUserStatus(Math.random), FETCH_LAST_ACTIVITY_TIMEOUT);
    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    if (getUserStatus) {
      getLastActivity();
    }
  }, [getUserStatus]);

  const getLastActivity = () => {
    if(!client || !connected) {
      clearInterval(interval);
      return;
    };
    client.getLastActivity(item.jid).then(res => {
      console.log(`ChatDataProvider getLastActivity res  ${item.address}`, {res});
      if(item.online != !res.seconds){
        console.log(`ChatDataProvider getLastActivity changed: ${item.address}`, !res.seconds);
        addActiveUser(item.address, !res.seconds)
      }
    }).catch(error => {
      console.log(`ChatDataProvider getLastActivity error ${item.address}`, error);
    })
  }

  return (
    <li
        onClick={props.onClick}
      className={`clearfix actve-user ${selected ? 'active' : ''}`}
    >
      <ChatUserAvatar/>
      <div className={`status-circle ${item.online ? 'online' : 'offline'}`}></div>
      <div className="about d-flex">
        <div className="mr-2">
          <div className="name">{item.username}</div>
          <div className="typing">{props.typing ? 'typing...' : ''}</div>
        </div>
        {unreadMessages ? <div className="mt-1 unread-message-count">{unreadMessages}</div> : null}
      </div>
    </li>
  );
};

export default UserItem;
