import React, { useEffect, useState } from 'react';
import { Media, TabPane, Spinner } from 'reactstrap';
import { useRef } from 'react';
import { getQueueChatUsers, userMapping } from 'services/chatService';
import { useChatData } from '../chatDataContext';
import ChatUserAvatar from '../userAvatar';
import { dataMessageBodyHint } from '../../../CustomerChatBoard';

const PendingUsers = (props) => {
  const { client, currentUser, onAddActiveUser } = useChatData();
  const [update, setUpdate] = useState(false);
  const [pendingLoading, setPendingLoading] = useState(false);
  const [intervalApiCall, setIntervalApiCall] = useState(false);
  const [mapping, setMapping] = useState(false);

  const pendingData = useRef([]);
  const pendingUsers = pendingData.current;

  useEffect(() => {
    const interval = setInterval(() => setIntervalApiCall(Math.random), 30000);
    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    if (intervalApiCall) {
      getPending();
    } else {
      getPending(true);
    }
  }, [intervalApiCall]);

  const getPending = (loading) => {
    setPendingLoading(loading);
    getQueueChatUsers()
      .then((res) => {
        const { data = {} } = res;
        const { users = [] } = data;
        const index = users.indexOf(currentUser.jid);
        pendingData.current = users;
        console.log('ChatUser PendingUsers getPending res', {res, index, currentUser});
        setPendingLoading(false);
      })
      .catch((error) => {
        console.log('ChatUser PendingUsers getPending error', error);
        setPendingLoading(false);
      });
  };

  const sendDataMessage = (to) => { // Init the chat
    const data = {
      from: currentUser.jid,
      type: 'init'
    }
    const msgData = {
      to,
      body: `${dataMessageBodyHint}${JSON.stringify(data)}`
    }
    client.sendMessage(msgData);
  }

  const startChat = (to) => {
    setMapping(to);
    userMapping(to, currentUser.jid)
      .then((res) => {
        acceptSubscription(to);
        client.subscribe(to);
        console.log('ChatUser startChat res', res);
        setMapping(false);
        removePendingUser(to);
        sendDataMessage(to);
        onAddActiveUser(to);
      })
      .catch((error) => {
        console.log('ChatUser startChat error', error);
        setMapping(false);
      });
  };

  const removePendingUser = (jid) => {
    const data = pendingData.current;
    const index = data.indexOf(jid);
    data.splice(index, 1);
    setUpdate(Math.random());
  };

  const acceptSubscription = (jid) => {
    client.acceptSubscription(jid);
  };

  return (
    <TabPane tabId="2">
      <div className="people-list">
        <ul className="list digits custom-scrollbar">
          {pendingLoading && !pendingUsers.length ? (
            <div className="d-flex justify-content-center mt-5 pt-5">
              <Spinner type="grow" color="primary" />{' '}
            </div>
          ) : null}
          {pendingUsers.map((member, index) => {
            return (
              <li key={index} className="clearfix" onClick={() => startChat(member)} style={{ cursor: 'pointer' }}>
                <ChatUserAvatar />
                <div className="about">
                  <div className="name" style={{ lineHeight: '44px' }}>
                    {member} {mapping === member ? <Spinner className="ml-1" size="sm" color="primary" /> : null}
                  </div>
                </div>
              </li>
            );
          })}
          {!pendingUsers.length && !pendingLoading ? (
            <div className="text-center mt-5">No have any user in queue.</div>
          ) : null}
        </ul>
      </div>
    </TabPane>
  );
};

export default PendingUsers;
