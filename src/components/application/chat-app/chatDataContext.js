import React, { useEffect, useState, useRef, useCallback } from 'react';
import { getJid, getUsername } from 'utils/filters';
import * as XMPP from 'stanza';
import { dataMessageBodyHint } from '../../CustomerChatBoard';
import _ from 'lodash';
import { toast } from 'react-toastify';

const ChatDataContext = React.createContext(null);

const ChatDataProvider = (props) => {
  const [clientData, setClient] = useState();
  const [connected, setConnected] = useState(false);
  const [currentUser, setCurrentUser] = useState({ jid: 'agent1@omannews.tk', password: 'agent1' });
  const [selectedUser, setSelectedUser] = useState();
  const [unreadMessages, setUnreadMessages] = useState({});
  const [activeUsers, setActiveUsers] = useState([]);
  const [activeAgents, setActiveAgents] = useState([]);
  const [fetchingChatLogs, setFetchingChatLogs] = useState(false);
  const [updateChatLogs, setUpdateChatLogs] = useState({});

  const activeDataRef = useRef({});
  const selectedUserRef = useRef();
  const clientRef = useRef();

  useEffect(() => {
    // connect(currentUser.jid, currentUser.password);
  }, []);

  const connect = (jid, password) => {
    if(connected){
      clientData.disconnect();
    };
    setCurrentUser({jid, password})
    
    const client = XMPP.createClient({
      jid,
      password,
      ...window.config.xmpp,
    });

    client.on('session:started', () => {
      console.log('ChatDataProvider session:started');
      setConnected(true);
      client.getRoster();
      client.sendPresence();
    });

    client.on('disconnected', (d) => {
      setConnected(false);
      console.log('ChatDataProvider disconnected', d);
    });

    client.on("auth:failed", d => {
      toast.error("Connection Failed, Please Try Again.")
    });

    client.on('subscribe', (d) => {
      console.log('ChatDataProvider subscribe', d);
    });

    client.on('unsubscribe', (d) => {
      console.log('ChatDataProvider unsubscribe', d);
      unsubscribeUser(getJid(d.from), client);
    });

    client.on('presence', (d) => {
      console.log('ChatDataProvider presence', d);
      if (getJid(d.from) !== jid && !["unavailable", "unsubscribe"].includes(d.type)) {
        client.subscribe(getJid(d.from));
        client.acceptSubscription(getJid(d.from));
        onAddActiveUser(d.from, client);
      }
    });

    client.connect();
    setClient(client);
    clientRef.current = client;
  };

  const addActiveUser = (address, online) => {
    const jid = getJid(address);
    const user = { jid, online, typing: false, address, username: getUsername(address) };
    activeDataRef.current[jid] = user;
    setActiveUsers(Object.values(activeDataRef.current));
  };

  const onAddActiveUser = (jid) => {
    console.log('ChatDataProvider onAddActiveUser top', {jid, isAlreadyAdded: activeDataRef.current[jid]});
    const client = clientRef.current;
    if(!client) return;
    client.getLastActivity(getJid(jid)).then(res => {
      addActiveUser(jid, !res.seconds)
      console.log('ChatDataProvider onAddActiveUser getLastActivity res', {res})
    }).catch(error => {
      addActiveUser(jid, false)
      console.log('ChatDataProvider onAddActiveUser getLastActivity error', error)
    })
  };

  const removeActiveUser = (jid) => {
    const data = {...activeDataRef.current};
    delete data[jid];
    activeDataRef.current = data;
    setActiveUsers(Object.values(activeDataRef.current));
    console.log('ChatDataProvider removeActiveUser', {jid, selectedUser: selectedUserRef.current, activeDataRef, array: Object.values(activeDataRef.current)})
    if(selectedUserRef.current && selectedUserRef.current.jid === jid){
      setSelectedUser('');
      selectedUserRef.current = '';
    }
  };

  const onSelectUser = (user) => {
    setSelectedUser(user);
    setUnreadMessages({...unreadMessages, [user.jid]: undefined});
    selectedUserRef.current = user;
  };

  useEffect(() => {
    console.log('ChatDataProvider selectUser update', {selectedUser, activeUsers})
    if(!selectedUser && activeUsers.length){
      onSelectUser(activeUsers[0]);
    }
  }, [activeUsers]);

  const unsubscribeUser = (jid, client) => {
    console.log('ChatDataProvider unsubscribeUser', {jid, client})
    client.unsubscribe(jid);
    removeActiveUser(jid);
  }

  const handleDataMessage = (body) => {
    const splitArr = body.split(dataMessageBodyHint);
    if(splitArr[1]){
      const data = JSON.parse(splitArr[1]) || {};
      if(data.from === currentUser.jid) return;
      if(data.type === 'unsubscribe'){
        unsubscribeUser(data.from, clientData);
      }
      console.log('ChatUser handleDataMessage data', { data });
    }
  }

  const sendDataMessage = (to, type) => { // Init the chat
    const data = {
      from: currentUser.jid,
      type,
    }
    const msgData = {
      to,
      body: `${dataMessageBodyHint}${JSON.stringify(data)}`
    }
    clientData.sendMessage(msgData);
  }

  const endChat = (jid) => {
    sendDataMessage(jid, 'end');
    unsubscribeUser(jid, clientData);
  }

  console.log('ChatUser ChatDataContext Provider Render', { selectedUser, activeDataRef });

  return (
    <ChatDataContext.Provider
      value={{
        client: clientData,
        connected,
        currentUser,
        selectedUser,
        unreadMessages,
        setUnreadMessages,
        connect,
        activeUsers,
        addActiveUser,
        setSelectedUser: onSelectUser,
        activeAgents,
        setActiveAgents,
        handleDataMessage,
        fetchingChatLogs,
        setFetchingChatLogs,
        updateChatLogs,
        setUpdateChatLogs,
        onAddActiveUser,
        endChat
      }}
      {...props}
    />
  );
};

const useChatData = () => React.useContext(ChatDataContext);

export default ChatDataContext;
export { ChatDataProvider, useChatData };
