import React, { useState } from 'react';
import Breadcrumb from 'layout/breadcrumb';
import { Container, Row, Col, Card, CardBody, Input, Button } from 'reactstrap';
import ActiveUsers from './activeUsers';
import RightSideContent from './rightContent';
import Messages from './messages';
import { ChatDataProvider, useChatData } from './chatDataContext';

export const xmppDomain = '@omannews.tk'

const Chat = (props) => {
  const [agentname, setAgentname] = useState('agent1');
  const { selectedUser, connect, connected } = useChatData();

  const onConnect = () => {
    connect(`${agentname}@omannews.tk`, agentname)
  }

  return (
    <>
      <Breadcrumb parent="Apps" title="Chat App" />
       {/* Ony For testing should remove */}
       <div className="col-4 mb-4">
        <Input
          type="text"
          className="form-control input-txt-bx mb-2"
          placeholder="Agent name"
          value={agentname}
          // onKeyPress={(e) => handleMessagePress(e)}
          onChange={(e) => setAgentname(e.target.value)}
        />
        <Button onClick={onConnect}>Connect</Button>
      </div>
      <Container fluid={true}>
        <Row>
          {connected ? <><ActiveUsers />
          <Col className="call-chat-body">
            <Card>
              <CardBody className="p-0">
                <Row className="chat-box">
                  <Messages selectedUser={selectedUser} />
                  <RightSideContent />
                </Row>
              </CardBody>
            </Card>
          </Col></> : null}
        </Row>
      </Container>
    </>
  );
};

export default (props) => {
  return (
    <ChatDataProvider>
      <Chat {...props} />
    </ChatDataProvider>
  );
};
