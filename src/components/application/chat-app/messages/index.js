import React, { useState, useRef, useEffect } from 'react';
import { Col, Media } from 'reactstrap';
import errorImg from 'assets/images/search-not-found.png';
import { isDataBody } from '../../../CustomerChatBoard';
import { getJid } from 'utils/filters';
import MessagesList from './messagesList';
import ActiveChatHeader from './userHeader';
import { useChatData } from '../chatDataContext';
import _ from 'lodash';
import { xmppDomain } from '..';
import MessageInputBox from './inputBox';

export const MESSAGE_MARKER = {
  MARKABLE: 'markable',
  RECEIVED: 'received',
  DISPLAYED: 'displayed',
};

export const MESSAGES_LOCALSTORAGE = 'chat_messages';

const Messages = (props) => {
  const { client, connected, currentUser = {}, unreadMessages, setUnreadMessages, handleDataMessage, updateChatLogs } = useChatData();
  const [update, setUpdate] = useState(false);

  const selectedUserRef = useRef();
  const unreadMessagesRef = useRef({});
  const selectedUser = selectedUserRef.current;

  const selectedJid = selectedUserRef.current ? selectedUserRef.current.jid : '';
  const allMessagesRef = useRef({});
  const messages = Object.values(allMessagesRef.current[selectedJid] || {});

const filterChatLogs = (logs=[], userJid) => {
  const newLogs = {};
  logs.map(l => {
    if(!isDataBody(l.body)){
      const isWithDomain = l.from.split('@').length === 2;
      const jid = isWithDomain ? l.from : `${l.from}${xmppDomain}`
      newLogs[l.id] = {...l, incoming: jid !== userJid};
    }
  });
  return newLogs;
}

  useEffect(() => {
    console.log('ChatDataProvider messages updateChatLogs top',{updateChatLogs});
    if (!updateChatLogs.jid) return;
    
    const filteredData = filterChatLogs(updateChatLogs.messages, updateChatLogs.jid);
    console.log('ChatDataProvider messages updateChatLogs',{updateChatLogs, filteredData});
      const all = allMessagesRef.current;
      const messages = all[updateChatLogs.jid] || {};
      allMessagesRef.current[updateChatLogs.jid] = {...filteredData, ...messages}
      setUpdate(Math.random());
  }, [updateChatLogs.jid]);

  useEffect(() => {
    if (!update) return;
    localStorage.setItem(MESSAGES_LOCALSTORAGE, JSON.stringify(allMessagesRef.current))
  }, [update]);

  useEffect(() => {
    const allPreviousMessages = localStorage.getItem(MESSAGES_LOCALSTORAGE);
    allMessagesRef.current = JSON.parse(allPreviousMessages || "{}") || {};
    console.log('Messages allPreviousMessages', allPreviousMessages, JSON.parse(allPreviousMessages || "{}"))
    setUpdate(null);
  }, [])

  useEffect(() => {
    if (props.selectedUser === undefined) return;
    selectedUserRef.current = props.selectedUser;
    unreadMessagesRef.current = unreadMessages;
    setUpdate(Math.random());
  }, [props.selectedUser, unreadMessages]);

  useEffect(() => {
    if (!connected) return;
    client.on('message', onMessage);
    client.on('message:sent', onSentMessage);
    client.on('marker:received', onReceipt);
    client.on('marker:displayed', onReceipt);
    // client.on('message:error', (d) => {
    //   console.log('ChatUser message:error', { d });
    // });
    // client.on('message:failed', (d) => {
    //   console.log('ChatUser message:failed', { d });
    // });
    // client.on('message:retry', (d) => {
    //   console.log('ChatUser message:retry', { d });
    // });
    // client.on('probe', (d) => {
    //   console.log('ChatUser probe', { d });
    // });
  }, [connected]);

  const onReceipt = (d) => {
    console.log('ChatUser Messages marker', { messages, d, currentUser });
    if (getJid(d.from) !== currentUser.jid) {
      const marker = d.marker || {};
      const userJid = getJid(d.from);
      const all = allMessagesRef.current;
      const messages = all[userJid] || {};
      const message = messages[marker.id];
      messages[marker.id] = { ...message, marker };
      setUpdate(Math.random());
    }
  };

  const onMessage = (msg = {}, notSent) => {
    if (!msg.body) return;
    if(isDataBody(msg.body)){
      handleDataMessage(msg.body);
      return;
    }
    const jid = getJid(msg.from);
    const currentJid = getJid(currentUser.jid);
    const userJid = jid === currentJid ? getJid(msg.to) : jid;
    const all = allMessagesRef.current;
    const messages = all[userJid] || {};
    messages[msg.id] = { ...msg, incoming: jid !== currentJid, notSent, timestamp: new Date() };
    allMessagesRef.current[userJid] = messages;
    setUpdate(Math.random());
    console.log('ChatUser messages onMessage', {
      msg,
      jid,
      userJid,
      selectedUserRef,
      currentUser,
      currentJid,
      all,
      notSent,
    });
    if (selectedUserRef.current && selectedUserRef.current.jid !== jid) {
      updateUnreadMessages(msg);
    }
  };

  const onSentMessage = (msg = {}) => {
    onMessage({ ...msg, from: currentUser.jid });
  };

  const updateUnreadMessages = (msg) => {
    const jid = getJid(msg.from);
    let unreadCount = unreadMessagesRef.current[jid] || 0;
    console.log('ChatUser messages updateUnreadMessages', { msg, jid });
    unreadCount += 1;
    unreadMessagesRef.current[jid] = unreadCount;
    setUnreadMessages(unreadMessagesRef.current);
  };

  console.log('ChatUsers messages Render', { messages, allMessagesRef, selectedUser, propsSelectedUser: props.selectedUser });

  return (
    <Col className="pr-0 chat-right-aside">
      {selectedUser ? (
        <div className="chat">
          <ActiveChatHeader {...props} />
          <MessagesList {...props} messages={messages} />
          <MessageInputBox onMessage={onMessage}/>
        </div>
      ) : (
        <div style={{ display: 'flex', height: '100%', alignItems: 'center', justifyContent: 'center' }}>
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <h3 style={{ color: 'gray' }}>No Active Users</h3>
            <Media className="img-fluid m-auto" src={errorImg} alt="" />
          </div>
        </div>
      )}
    </Col>
  );
};

export default Messages;
