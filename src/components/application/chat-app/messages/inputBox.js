import React, { useState, useRef, useCallback } from 'react';
import { Row, Col, Media, Input, InputGroup, InputGroupAddon, Button, Form } from 'reactstrap';
import { Picker } from 'emoji-mart';
import { Send } from '../../../../constant';
import { CHATSTATES } from '../../../CustomerChatBoard';
import { v4 as uuidv4 } from 'uuid';
import { useChatData } from '../chatDataContext';
import _ from 'lodash';

export const MESSAGE_MARKER = {
  MARKABLE: 'markable',
  RECEIVED: 'received',
  DISPLAYED: 'displayed',
};

export const MESSAGES_LOCALSTORAGE = 'chat_messages';

const MessageInputBox = (props) => {
  const { client, currentUser = {}, selectedUser } = useChatData();
  const [messageInput, setMessageInput] = useState('');
  const [showEmojiPicker, setShowEmojiPicker] = useState(false);

  const typingTimerRef = useRef();

  const toggleEmojiPicker = () => {
    setShowEmojiPicker(!showEmojiPicker);
  };

  const addEmoji = (emoji) => {
    const text = `${messageInput}${emoji.native}`;
    setShowEmojiPicker(false);
    setMessageInput(text);
  };

  const handleMessageChange = (message) => {
    setMessageInput(message);
    clearTimeout(typingTimerRef.current);
    setTypingTimeout();
    if(message.length === 1){
      sendChatState(CHATSTATES.COMPOSING, selectedUser.jid);
    } else {
      sendChatStateDebounce(CHATSTATES.COMPOSING, selectedUser.jid);
    }
  };

  const setTypingTimeout = () => {
    typingTimerRef.current = setTimeout(() => {
      sendChatStateDebounce(CHATSTATES.PAUSED, selectedUser.jid);
    }, 5000);
  };

  const sendChatStateDebounce = useCallback(
    _.debounce((state, to) => sendChatState(state, to), 500),
    [],
  );

  const sendChatState = (state, to) => {
    client.sendMessage({
      chatState: state,
      to,
    });
  };

  const sendMessage = (e) => {
    e.preventDefault();
    if (!messageInput.trim()) return;
    const message = {
      to: selectedUser.jid,
      body: messageInput,
      marker: { type: MESSAGE_MARKER.MARKABLE },
      id: uuidv4(),
      type: 'chat',
    };
    client.sendMessage(message);
    props.onMessage({ ...message, from: currentUser.jid }, true);
    setMessageInput('');
    clearTimeout(typingTimerRef.current);
    sendChatStateDebounce(CHATSTATES.PAUSED, selectedUser.jid);
  };

  return (
    <div className="chat-message clearfix">
        <Row>
            <div className="mb-2">
                {showEmojiPicker ? <Picker set="apple" emojiSize={30} onSelect={addEmoji} /> : null}
            </div>
            <Col xl="12" className="d-flex">
                <div className="smiley-box bg-primary">
                    <div className="picker" onClick={() => toggleEmojiPicker()}>
                        <Media src={require('assets/images/smiley.png')} alt="" />
                    </div>
                </div>
                <Form onSubmit={sendMessage} className="" style={{flex: 1}}>
                    <InputGroup className="text-box">
                        <Input
                            type="text"
                            className="form-control input-txt-bx"
                            placeholder="Type a message......"
                            value={messageInput}
                            onChange={(e) => handleMessageChange(e.target.value)}
                        />
                        <InputGroupAddon addonType="append">
                            <Button color="primary" type="submit">
                            {Send}
                            </Button>
                        </InputGroupAddon>
                    </InputGroup>
                </Form>
            </Col>
        </Row>
    </div>
  );
};

export default MessageInputBox;
