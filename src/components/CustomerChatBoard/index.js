import React, { useState, useEffect, useRef } from "react";
import { MessageSquare, X } from "react-feather";
import {
  Popover,
  PopoverHeader,
  PopoverBody,
  Form,
  FormGroup,
  Input,
  Button,
} from "reactstrap";
import { Spinner } from 'reactstrap';
import Iframe from 'react-iframe';
import useWindowSize from "../../hooks/useWindowSize";

export const CHATSTATES = {
  COMPOSING: 'composing',
  PAUSED: 'paused',
}

export const FETCH_LAST_ACTIVITY_TIMEOUT = 90000;

export const dataMessageBodyHint = "__!data__";

export const isDataBody = (body) => {
  const splitArr = body.split(dataMessageBodyHint);
  return !!splitArr[1];
}

const CustomerChatBoard = () => {
  const [chat, setChat] = useState(false);
  const [emailPopup, setEmailPopup] = useState(false);
  const [username, setUsername] = useState("");
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  const size = useWindowSize() || {};
  const isMobile = size < 500;
  
  const openEmail = () => {
    setChat(true);
  };
  useEffect(() => {
    const handler = event => {
      const data = JSON.parse(event.data || "{}")
      if(data.chatbotClose){
        handleClose();
      }
    }

    window.addEventListener("message", handler)
    return () => window.removeEventListener("message", handler)
  }, [])

  const handleClose = () => {
    setChat(false);
    // logout();
  };

  const register = () => {
   
  }

  const handleConfirm = (e) => {
    e.preventDefault();
    register();
  };

  const handleUsernameChange = (e) => {
    setError('');
    setUsername(e.target.value);
  };


  console.log('isMobileisMobileisMobile', {isMobile, size})
  return (
    <>
      <div className="customChatboardBody">
        <div
          className="customChatboard chatbot-content"
          style={{
            position: "fixed",
            bottom: 40,
            right: 40,
            backgroundColor: "#ed222b",
            padding: "15px 15px 7px 15px",
            borderRadius: 25,
          }}
          id="Popover1"
        >
          <div className="onhover-dropdown">
            <MessageSquare fill="#fff" color="#fff" />
          </div>
        </div>
          <div
            className="chatComponent"
            style={{
              position: "fixed",
              bottom: isMobile ? 0 : 30,
              right: isMobile ? 0 : 30,
              width: chat ? isMobile ? '100%' : 350 : 0,
              transition: '0.3s all ease',
              height: isMobile ? '100%' : 'unset',
            }}
          >
            <div
              className="activeCustomerName"
              style={{
                backgroundColor: "#ed222b",
                padding: "0 10px",
                display: "flex",
                justifyContent: "space-between",
                fontSize: "calc(8px + 1vmin)",
                color: "#fff",
                width: isMobile ? 350 : '100%',
                alignItems: "center",
                overflow: 'hidden'
              }}
            >
            </div>
            <Iframe url="https://qr-mlkit.firebaseapp.com/"
              width={isMobile ? "100%" : "350px"}
              height={chat ? isMobile ? "100%" : '600px' : "0px"}
              id="myId"
              className={`chat-bot-iframe ${chat ? 'chat-bot-iframe-border' : ''}`}
              display="initial"
              position="relative"
              frameBorder={chat ? null : 0}
            />
          </div>
      </div>
      {/* username popup */}
      <Popover
        placement="auto"
        isOpen={emailPopup}
        target="Popover1"
        toggle={openEmail}
      >
        <PopoverHeader style={{ background: "#ed222b" }}>
          Confirm Your Email Id
        </PopoverHeader>
        <PopoverBody>
          <Form onSubmit={handleConfirm}>
            <FormGroup>
              <Input
                autoComplete="off"
                type="name"
                name="username"
                value={username}
                id="name"
                placeholder="Enter your name"
                style={{ marginBottom: 10, width: "100%" }}
                onChange={handleUsernameChange}
              />
              {error ? <div>{error}</div> : null}
            </FormGroup>
            <Button disabled={username.length < 3 || loading} color="primary" type="submit">
              {loading ? <Spinner size="sm" color="secondary" /> : 'Submit'}
            </Button>
            <Button
              disabled={loading}
              color="secondary"
              style={{ margin: "0 5px" }}
              onClick={openEmail}
            >
              Cancel
            </Button>
          </Form>
        </PopoverBody>
      </Popover>
    </>
  );
};

export default CustomerChatBoard;
