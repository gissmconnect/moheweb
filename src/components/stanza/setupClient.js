import * as XMPP from "stanza";
// import configureClient from "../../xmpp";

function setupClient() {
  const client = XMPP.createClient({
    ...window.config.xmpp
  });

  window.client = client;
  // configureClient(client);

  client.on("session:started", () => {
    client.getRoster();
    client.sendPresence();
  });

  // client.on("raw:incoming", console.log);
  // client.on("raw:outgoing", console.log);

  return client;
}

export default setupClient;
