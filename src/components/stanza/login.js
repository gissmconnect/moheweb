import React from 'react';

class Login extends React.Component {
    render() {
        console.log('login render', this.props)
        return (
        <div style={{display: 'flex', justifyContent: 'center', alignContent: 'center', marginTop: 20}} className="Home Page">
            <div>
            <h2>Connect</h2>
            <form className="form" onSubmit={this.handleSubmit}>
                <div>
                <label style={{width: 100}} htmlFor="jid">jid</label>
                <input ref={el => this._jid = el} type="text" name="jid" id="jid" placeholder="Username" />
                </div>
                <div>
                <label style={{width: 100}} htmlFor="password">Password</label>
                <input ref={el => this._password = el} name="password" id="password" placeholder="Password" />
                </div>
                <div className="actions">
                    <input type="submit" value={this.props.loading ? "Connecting..." : "Connect"} />
                </div>
            </form>
            </div>
        </div>
        );
    }

    handleSubmit = e => {
        e.preventDefault();
        if(this._jid.value.length && this._password.value.length) {
            this.props.login(this._jid.value, this._password.value);
        }
    };

}

export default Login;