import React, { Fragment, useEffect, useState, Component, forwardRef } from 'react';
import Breadcrumb from '../../layout/breadcrumb'
import { Container, Row, Col, Card, CardHeader, CardBody, CardFooter, Media, Form, FormGroup, Label, Input, Button } from 'reactstrap'
import { AddBox, ArrowDownward } from "@material-ui/icons";
import MaterialTable, { MTableEditField } from "material-table";
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import CircularProgress from "@material-ui/core/CircularProgress";
import { getKbCategory, addCategory, updateCategory, deleteCategory } from "services/kbService";
import { toast } from 'react-toastify';

const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

const KbCategory = (props) => {
    const [categoryList, setCategoryList] = useState([])
    useEffect(() => {
        getCategoryList();
    }, []);
    const getCategoryList = async () => {
        try {
            let res = await getKbCategory();
            if (res.length > 0) {
                setCategoryList(res)
            }
        } catch (error) {
            console.log(error)
        }
    };
    const handleRowUpdate = async (newData, oldData, resolve, reject) => {
        let errorList = [];
        if (!newData.category) {
            errorList.push('category is required')
            toast.error('Category is Required')
            reject();
        }
        if (errorList.length === 0) {
            try {
                await updateCategory(
                    newData.category,
                    newData._id
                )
                getCategoryList();
                resolve()
            } catch (error) {
                console.log(error)
                resolve()
            }
        } else {
            reject()
        }
    }
    const handleRowAdd = async (newData, resolve, reject) => {
        let errorList = [];
        if (!newData.category) {
            errorList.push('category is required')
            toast.error('Category is Required')
            reject();
        }
        if (errorList.length === 0) {
            try {
                await addCategory(newData.category)
                getCategoryList();
                resolve()
            }
            catch (error) {
                console.log(error)
                resolve()
            }
        } else {
            reject()
        }
    }
    const handleRowDelete = async (oldData, resolve) => {
        try {
            await deleteCategory(oldData._id)
            getCategoryList();
            resolve()
        } catch (error) {
            console.log(error)
            resolve()
        }
    }
    return (
        <Fragment>
            <Breadcrumb parent="Apps" title="KB Category" />
            <Container fluid={true}>
                <div className="edit-profile">
                    <Row>
                        <Col md="12">
                            <MaterialTable
                                icons={tableIcons}
                                columns={[
                                    {
                                        title: "Category", field: "category", headerStyle: {
                                            zIndex: 0
                                        }
                                    },
                                    {
                                        title: "ID",
                                        field: "_id",
                                        hidden: true
                                    },
                                ]}
                                data={categoryList.map((item) => {
                                    return {
                                        category: item.type,
                                        _id: item._id
                                    }
                                })}
                                title=""
                                options={{
                                    actionsColumnIndex: -1,
                                    addRowPosition: 'first'
                                }}
                                editable={{
                                    onRowUpdate: (newData, oldData) =>
                                        new Promise((resolve, reject) => {
                                            handleRowUpdate(newData, oldData, resolve, reject);
                                        }),
                                    onRowAdd: (newData) =>
                                        new Promise((resolve, reject) => {
                                            handleRowAdd(newData, resolve, reject)
                                        }),
                                    onRowDelete: (oldData) =>
                                        new Promise((resolve) => {
                                            handleRowDelete(oldData, resolve)
                                        }),
                                }}
                            />
                        </Col>
                    </Row>
                </div>
            </Container>
        </Fragment>
    );
}

export default KbCategory;