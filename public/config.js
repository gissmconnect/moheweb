(function () {
  var boshURL = "https://omannews.tk/bosh/";

  window.config = {
    xmpp: {
      transports: {
        // websocket: "wss://example.com:5281/xmpp-websocket",
        bosh: boshURL,
      }
    }
  };
  
})();
